{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Stackoverflow workers dataset presentation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stackoverflow (https://stackoverflow.com/) is a widely known Q&A service specialized in computer science. We propose here to use the public dataset of Stackoverflow for generating real-life skills profiles. We study then the unicity of users with respect to their skills profiles in order to evaluate the identifying power of detailed skills profiles. We also plan to consider this dataset as a set of worker profiles to be used in the experimental evaluation of crowdsourcing techniques. We use below the term \"worker\" or \"user\" interchangeably. First, we present the data extracted from Stackoverflow and the workers generation method. Then we present the skills distribution and study the unicity of the skills profiles of workers. Unique workers can easily be linked to actual StackOverflow\n",
    "users or may even be linked to individuals. Our objective is to clearly understand the impact that these users might have on our dataset for privacy reason. Finally, we will conclude by summarising the workers set properties previously defined."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The dataset\n",
    "\n",
    "### The stackoverflow public dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Stackoverflow lets the public access all of their data, either through an interactive SQL interface (https://data.stackexchange.com/stackoverflow/queries) or by downloading the datasets in XML format (https://archive.org/download/stackexchange). Each file represents a data table.\n",
    "\n",
    "The interactive SQL interface does not allow long requests to be made, so we chosed to download the XML datasets. To generate our workers, we downloaded the following files : stackoverflow.com-Posts.7z, stackoverflow.com-Tags.7z and stackoverflow.com-Votes.7z\n",
    "\n",
    "The XML files are heavy and contains various information. We kept only the information relevant to our study and converted the files to CSV. The following schema describes the information kept in our CSV files and the relationships between them.\n",
    "![uml](./so_uml.svg)\n",
    "\n",
    "The Votes tables contains a lot of rows (around 160 millions), where each row represents a single vote. We reduced this table by summing, for each post the number of up-votes and down-votes. These sums are then added as two columns to the Posts table. This results in reducing the size of the data and speed up further computation. We are only interested in the votes as a sum per posts and not individually. \n",
    "\n",
    "Note that even if a table that links *Posts* to *Tags* exist in the interactive format, it is not available to download. The *Tags* field in the *Posts* table contains the tags name in a text field under the following schema : \"`<tag 1><tag 2>`\" (ex : \"`<git><github>`\") and not the tag Id, easier to manipulate. So we re-created the *PostTags* table (done by mapping the tags present in the *Tags* field).\n",
    "\n",
    "Theses transformations results in the upgraded data schema below:\n",
    "![uml](./so_cleaned_uml.svg)\n",
    "\n",
    "A few facts : with the current dataset we have more than 10 millions users, 44 millions posts and around 54000 tags. The four most used tags are Javascript, Java, C# and PHP.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Skills Profiles Generation Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We considered that the set of skills is the set of tags. We did not\n",
    "consider all the tags, but only up to 50 tags (selected as described\n",
    "below). We considered then that users are workers. Based on their\n",
    "answers to posts we are able to extract their skills vector. Several\n",
    "elaborate methods can be used for automatically computing the degree\n",
    "of competency of a given worker for a given skill (see for example https://link.springer.com/content/pdf/10.1007%2F978-3-642-37487-6_5.pdf). Since rating users\n",
    "is not the focus of this work, we preferred to implement a simple\n",
    "naive popularity score based on the up-votes associated to posts\n",
    "(formula is below)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More precisely, in order to generate workers, we chosed some tags (recall that a tag represents a skill), got their ID and filtered all the PostTags table to keep only the lines with theses IDs.  \n",
    "Thoses lines contains only the threads ID (considered as the first post, i.e., the question) related to our tags, not all the answers. We filter then all the posts related to these\n",
    "threads (the questions through the *Posts* *Id* field,\n",
    "and the answers through the *Posts* *ParentId*\n",
    "field). Finally, we compute a popularity score for the post based on\n",
    "the votes. The higher the score the more this post has been praised by\n",
    "other users. This gives an indication on the degree of competency of\n",
    "the associated user. We compute it as follows :\n",
    "```\n",
    "score = number of up votes / total number of votes\n",
    "```\n",
    "We score each post, per user and per tag, and we compute for each user the average score about each tag he has posted about. We have then a set of users (our workers) with each of them having a competency level\n",
    "between 0 and 1 for each of our tags. All the users who have no skills (all tags have a score of 0) are removed.\n",
    "\n",
    "The generated workers set is then stored in a CSV file with the following columns : the unique user id, the tag and the score."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Example with two users (1 and 2) and two tags :\n",
    "\n",
    "| user id | tag id | skill level |\n",
    "|:----------|:----------|:----------|\n",
    "| 1 | 1 | 0.2 |\n",
    "| 1 | 2 | 0.7 |\n",
    "| 2 | 1 | 0.3 |\n",
    "| 2 | 2 | 0.0 |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definitions\n",
    "\n",
    "- Precision : We consider below precision levels of 0, 1, and 2. With a precision level of 0, the degree of competency is either 0 or 1. With a precision level of 1, the degree of competency is rounded to the first decimal after 0. And with a precision level of 2, it is rounded to the second decimal after 0. For example, a raw\n",
    "  degree of competency of 0.42 is rounded to 0 at precision level 0. For a precision of 1, a raw degree of competency of 0.42 is rounded to 0.4.\n",
    "- Fingerprint : For a user, his fingerprint is a vector that indicates for each tag the rounded degree of competency. For example, for the user 1 of the previous example and a precision of 0, the fingerprint is : `{1:0,2:1}`.\n",
    "- Unique user : A user is considered unique if no other user has the same fingerprint.\n",
    "- Unicity ratio : Ratio of unique users :  \n",
    "```\n",
    "ratio = number of unique users / total number of users\n",
    "```\n",
    "- Group : A group of tags represents, for a given user, all the tags with degree of competency above 0.\n",
    "- Group size : The exact number of tags in a group (a user with exactly 2 tags above 0 will have a group size of 2).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Statistics considered\n",
    "For a given dataset and for a given precision we compute different\n",
    "kinds of statistics. First we compute statistics globally per tag,\n",
    "such as the average ratio per tag and the degree of competency\n",
    "distribution withing each tag. Then, we compute statistics at the\n",
    "group size level. For each group size, we compute statistics related\n",
    "to this group size : number of groups, of fingerprints, of unique\n",
    "users. The average of each statistic of each group is then performed\n",
    "to give the total number of unique users.\n",
    "\n",
    "The statistics per group size are then agregated to give global\n",
    "statistics. We keep the total number of users and the total number of\n",
    "unique users."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Workers set generated\n",
    "\n",
    "We generated worker sets for 4, 10 and 50 tags. For 4 tags, we took the 4 most prolific (with the most threads) tags (i.e, JS, Java, C# and PHP), for 10 and 50 tags, we took respectively the first 10 and 50 tags in the tag table.\n",
    "\n",
    "For example, the 10 first tags (index, IDs, tag name and the number of threads) are : \n",
    "```\n",
    "0,1,.net,280415\n",
    "1,2,html,806983\n",
    "2,3,javascript,1769208\n",
    "3,4,css,575590\n",
    "4,5,php,1265522\n",
    "5,8,c,297410\n",
    "6,9,c#,1289429\n",
    "7,10,c++,606864\n",
    "8,12,ruby,202677\n",
    "9,14,lisp,5683\n",
    "\n",
    "```\n",
    "\n",
    "\n",
    "On these workers set, we have calculated statistics for a precision level of 0, 1, and 2."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global statistics per tag\n",
    "\n",
    "Heatmap representing the degree of competency distribution per tags. It has been generated for 10 tags and a precision of 1.\n",
    "\n",
    "![heatmap](./heatmap.svg)\n",
    "\n",
    "It shows essentially that, for all the tags, most users have no competency about them (peak at 0). The rest of the distribution is not visible on this graph, so, we plot below the same graph but excluding, for each tag, the users having 0 competency in it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![heatmap_wz](./heatmap_wz.svg)\n",
    "\n",
    "This shows that, the big peak at zero left aside, the users are\n",
    "separated in two groups: a first group around 0.5 and a second group\n",
    "at 1. As a result we can distinguish, for each tag, three groups of\n",
    "users : a main group of users who do not have the skill (with a degree\n",
    "of competency between 0.0 and 0.1), a small group of medium skilled\n",
    "workers (spreading around a degree of competency of 0.5) and the\n",
    "experts (with a degree of competency above 0.9)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Global unicity ratio\n",
    "Percentage of unique users for different numbers of tags and different fingerprint. This figure was generated with a set of workers of 4, 10 and 50 tags and, for each set, a precision of 0, 1 and 2.\n",
    "\n",
    "![global](./global.png)\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This figure show the impact of the number of tags on the unicity of users. Even with a low fingerprint precision, with a high number of tags the proportion of unique users is high."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Unicity evolution per group size\n",
    "\n",
    "Evolution of the unicity ratio per group size for a dataset of 10 tags and various precision levels.\n",
    "\n",
    "![unicity per group](./evol10.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With 10 tags and a precision of 0, we can observe that groups always\n",
    "contain non-unique users. However, this observation does not hold\n",
    "anymore for higher precision levels. Moreover, the higher the\n",
    "precision level the smaller the minimum group size such that the\n",
    "groups contain unique users only (e.g., group size of 7 for a\n",
    "precision of 1, group size of 6 for a precision of 2)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The slight unicity drop for groups of size 10 seems to be neglected when the number of tags is increased as shown in the following figure (with 50 tags):\n",
    "![evol50](./evol50.svg)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With 50 tags, the same applies : the higher\n",
    "the precision and the smaller the minimum group size such that the\n",
    "groups contain unique users only."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Number of users per group size\n",
    "\n",
    "Evolution of the total number of users compared to the number of unique users per group size.  This figure has been generated for a dataset of 10 tags and a precision of 1.\n",
    "\n",
    "![nusers](./nusers.svg)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This figure confirms our previous observation that a group size of 6\n",
    "is sufficient for making all users unique in all the different groups\n",
    "of 6 tags. However, only a relatively small number of users is\n",
    "concerned. Indeed, most users have less than 3 skills. This is why,\n",
    "for 10 tags and a precision of 1, the proportion of unique users is\n",
    "slightly less than 5%. This figure also shows that a majority of\n",
    "users are mono-skilled."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "We can conclude from our study that most users are mono-skilled, and,\n",
    "if they have a skill they have higher chance to be expert or midly\n",
    "skilled.  A specialized worker (with a single skill) is not likely to\n",
    "be unique, contrarily to polyvalent workers. The proportion of unique\n",
    "users indeed grows with the number of skills possessed. Despite the\n",
    "fact that, given the scale of the dataset the proportion looks small,\n",
    "it concerns a large number of users (large absolute value)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
