/// Get the interval index for the value with the given split number.
pub fn split_index(value: f64, splits: usize) -> usize {
    let splits_range = 1. / (splits as f64);

    if value == 0. {
        return 0;
    } else {
        for i in 0..splits{
            if (i as f64 * splits_range) < value && value <= ((i + 1) as f64 * splits_range) {
                return i;
            }
        }
    }

    panic!("Unable to extract the split index for the value : {} and the splits {}", value, splits);
}