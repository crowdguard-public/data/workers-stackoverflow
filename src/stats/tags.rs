use rayon::prelude::*;
use serde::Serialize;
use crate::{
    generation::{RawWorkersSet, WorkersSet},
    stats::misc::split_index
};

#[derive(Debug, Serialize)]
pub struct TagsStats {
    tag_id: u64,
    avg_ratio_nz: f64,
    avg_ratio_z: f64,
    min_ratio: f64,
    max_ratio: f64,
    count: usize,
    frequency: Vec<f64>
}


/// Calculate statistics on the ratio results per tags.
///
/// * `ratios` - Ratios per user and per tags
/// * `splits` - Number of points for the frequency analysis, splits > 0
///
/// Result : Vec<(tag id, avg ratio, count, min ratio, max ratio, Vec<frequency in the range>
pub fn stats_tags_bis(ratios: &mut RawWorkersSet, tags: &[u64], splits: usize) -> Vec<TagsStats> {
    let mut stats = vec![];
    let flen = ratios.0.len() as f64;

    for tag in tags {
        // count number of non zero ratios
        let nz = ratios.0.par_iter()
            .filter(|(_, t, _)| t == tag)
            .filter(|(_, _, r)| r > &0.)
            .count();
        let fnz = nz as f64;

        // sum of all ratios for this tag
        let ratio = ratios.0.par_iter()
            .filter(|(_, t, _)| t == tag)
            .fold(|| 0., |acc, (_, _, r)| acc + r)
            .sum::<f64>();

        let freq: Vec<Vec<usize>> = ratios.0.par_iter()
            .filter(|(_, t, _)| t == tag)
            .fold(|| vec![0; splits], |mut acc, (_, _, r)| {
                acc[split_index(*r, splits)] += 1;
                acc
            })
            .collect();
        let freq: Vec<f64> = freq.into_iter()
            .fold(vec![0; splits], |mut acc, vals| {
                for i in 0..splits {
                    acc[i] += vals[i]
                }
                acc
            })
            .drain(..)
            .map(|x| x as f64 / flen)
            .collect();

        stats.push(TagsStats {
            tag_id: *tag,
            avg_ratio_nz: ratio / fnz,
            avg_ratio_z: ratio / flen,
            min_ratio: 0.,
            max_ratio: 1.,
            count: nz,
            frequency: freq
        })
    }

    stats
}


/// Calculate statistics on the ratio results per tags.
///
/// * `ratios` - Ratios per user and per tags
/// * `splits` - Number of points for the frequency analysis, splits > 0
///
/// Result : Vec<(tag id, avg ratio, count, min ratio, max ratio, Vec<frequency in the range>
pub fn stats_tags(ratios: &mut RawWorkersSet, splits: usize) -> Vec<TagsStats> {
    ratios.0.par_sort_by(|(_, a, _), (_, b, _)| a.cmp(b));

    let mut stats = vec![];
    let mut tag = 0;
    let mut ratio = 0.;
    let mut n = 0;
    let mut count = 0;
    let mut min = 1.;
    let mut max = 0.;
    let mut freq = vec![0; splits];


    for (_, t, r) in &ratios.0 {
        let r = *r;

        if tag == *t {
            ratio += r;
            n += 1;

            if r < min {
                min = r;
            }

            if r > max {
                max = r;
            }

            if r > 0. {
                count += 1;
            }

            freq[split_index(r, splits)] += 1;
        } else {
            if tag != 0 {
                stats.push(TagsStats{
                    tag_id: tag,
                    avg_ratio_nz: 0.,
                    avg_ratio_z: ratio / n as f64,
                    min_ratio: min,
                    max_ratio: max,
                    count: count,
                    frequency: freq.iter().map(|x| *x as f64 / n as f64).collect()
                });
            }

            tag = *t;
            ratio = r;
            n = 1;
            min = r;
            max = r;
            count = 0;

            freq = vec![0; splits];
        }
    }

    // Push the last element
    stats.push(TagsStats{
        tag_id: tag,
        avg_ratio_nz: 0.,
        avg_ratio_z: ratio / n as f64,
        min_ratio: min,
        max_ratio: max,
        count: count,
        frequency: freq.iter().map(|x| *x as f64 / n as f64).collect()
    });

    stats
}