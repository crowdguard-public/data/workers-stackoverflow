use std::collections::HashSet;
use rayon::prelude::*;
use serde::Serialize;

use crate::{
    generation::WorkersSet,
    stats::misc::split_index
};
use std::cmp::Ordering::Equal;

#[derive(Debug, Serialize)]
pub struct GroupSizeStats {
    /// Number of skills mastered
    gsize: usize,
    /// Number of users with this number of skills
    nusers: usize,
    /// Average ratio
    avg_ratio: f64,
    /// Total number of fingerprints
    fingerprints: usize,
    /// Average number of users per fingerprints
    avg_fingerprints: usize,
    /// Number of fingerprint with only one user
    unique_fingerprints: usize,
    /// Ratio (unique_fingerrpints / fingerprints)
    unique_ratio: f64,
    /// Get the ratios quartiles
    quartiles_ratio: Vec<f64>,
    /// Get the number of users quartiles
    quartiles_nusers: Vec<usize>
}

/// Statistics on the fingerprints for a group of tags
struct FingerprintsStats {
    /// Number of fingerprint
    n_fingerprint: usize,
    /// Total number of users for this group of tags
    n_users: usize,
    /// Average number of user per fingerprint
    avg_users: f64,
    // /// Average ratio for this group of tags
    // avg_ratio: f64,
    /// Number of unique fingerprint (only one user)
    n_unique: usize,
    /// Ratio of unique users (n_unique / n_fingerprint)
    ratio_unique: f64,
    /// Average skills ratio
    avg_ratio: f64
}

/// Statistics on the groups for a certain group size
#[derive(Debug, Serialize)]
pub struct GroupsSizeStats_bis {
    /// Group size
    gsize: usize,
    /// Total number of users
    n_users: usize,
    /// Quartiles information on the number of users per groups
    quartiles_nusers: Vec<usize>,
    /// Number of tags groups
    n_group: usize,
    /// Average ratio for this group size
    avg_ratio: f64,
    /// Quartiles information on the ratios
    quartiles_ratio: Vec<f64>,
    /// Average number of users per group
    avg_users_group: f64,
    /// Total number of fingerprints
    n_fingerprint: usize,
    /// Average number of fingerprints for the group size
    avg_fingerprint: f64,
    /// Total number of unique fingerprints
    n_unique: usize,
    /// Average number of unique users per groups
    avg_unique: f64,
    /// Quartiles information on the number of unique ratios per group
    quartiles_unique: Vec<f64>
}


/// Calculate the group size (number of skills > 0)
fn group_size(skills: &Vec<(u64, f64)>) -> usize {
    skills.par_iter().filter(|(t, r)| r > &0.).count()
}

/// Get the frequency fingerprint per user skills distribution.
/// For each tags (supposed that skills is sorted by increasing tags).
fn freq_group_fingerprint(skills: &Vec<(u64, f64)>, splits: usize) -> String {
    let mut fingerprint = "".to_string();

    for (_, ratio) in skills {
        let si = split_index(*ratio, splits);
        fingerprint += &format!("{} ", si);
    }

    fingerprint
}

/// Get, in a string all the tags above 0 for this skills set.
fn freq_group(skills: &Vec<(u64, f64)>) -> String {
    let mut fingerprint = "".to_string();

    for (tag, ratio) in skills {
        if ratio > &0. {
            fingerprint += &format!("{} ", tag);
        }
    }

    fingerprint
}

/// Get the quartiles of a list of usize values
fn quartiles_usize(mut values: Vec<usize>) -> Vec<usize> {
    if values.is_empty() {
        values.push(0);
    }

    let mut quartiles = vec![];

    values.par_sort_by(|a, b| a.cmp(b));

    // min value
    quartiles.push(values[0]);

    // the 3 quartiles
    for i in 1..3 {
        let mut i = i * values.len() / 4;
        if i >= values.len() {
            i = values.len() - 1;
        }

        quartiles.push(values[i]);
    }

    // Max value (end of the 4 quartile)
    quartiles.push(values[values.len() -1]);

    quartiles
}


/// Get the quartiles of a list of usize values
fn quartiles_f64(mut values: Vec<f64>) -> Vec<f64> {
    if values.is_empty() {
        values.push(0.);
    }

    let mut quartiles = vec![];

    values.par_sort_by(|a, b| a.partial_cmp(b).unwrap_or(Equal));

    // min value
    quartiles.push(values[0]);

    // the 3 quartiles
    for i in 1..3 {
        let mut i = i * values.len() / 4;
        if i >= values.len() {
            i = values.len() - 1;
        }

        quartiles.push(values[i]);
    }

    // Max value (end of the 4 quartile)
    quartiles.push(values[values.len() -1]);

    quartiles
}


/// Get the four quartiles
fn calc_quartiles(mut values: Vec<(f64, usize)>) -> (Vec<f64>, Vec<usize>) {
    if values.is_empty() {
        values.push((0., 0));
    }

    // Calculate the quartiles per average ratios
    let mut q_avg = vec![];
    values.par_sort_by(|(a, _), (b, _)| a.partial_cmp(b).unwrap_or(Equal));

    // min value
    q_avg.push(values[0].0);

    // the 3 quartiles
    for i in 1..3 {
        let mut i = i * values.len() / 4;
        if i >= values.len() {
            i = values.len() - 1;
        }

        q_avg.push(values[i].0);
    }

    // Max value (end of the 4 quartile)
    q_avg.push(values[values.len() -1].0);

    // Calculate the quartiles per number of users
    let mut q_nusers = vec![];
    values.par_sort_by(|(_, a), (_, b)| a.cmp(b));
    q_nusers.push(values[0].1);
    for i in 1..3 {
        let mut i = i * values.len() / 4;
        if i >= values.len() {
            i = values.len() - 1;
        }

        q_nusers.push(values[i].1);
    }
    q_nusers.push(values[values.len() -1].1);


    (q_avg, q_nusers)
}

/// Add fingerprint information on the workers set
/// Vec<(user id, Vec<(tag id, ratio)>, tags above 0, avg ratios of all skills)>
fn anotate_fingerprint(workers: Vec<(i64, Vec<(u64, f64)>, String, f64)>, splits: usize) -> Vec<(i64, Vec<(u64, f64)>, String, String, f64)> {
    workers.into_par_iter()
        .map(|(u, skills, g, a)| {
            let fp = freq_group_fingerprint(&skills, splits);
            (u, skills, fp, g, a)
        })
        .collect()
}

/// Get global stats on the fingerprints of a group of tags
fn stats_fingerprint(workers: Vec<(i64, Vec<(u64, f64)>, String, f64)>, size: usize, splits: usize) -> FingerprintsStats{
    let mut workers = anotate_fingerprint(workers, splits);

    let mut stats = FingerprintsStats {
        n_fingerprint: 0,
        n_users: 0,
        avg_users: 0.,
        n_unique: 0,
        ratio_unique: 0.,
        avg_ratio: 0.
    };

    while let Some(worker) = workers.pop() {
        let (mut left, right): (Vec<_>, Vec<_>) = workers.into_par_iter().partition(|(_, _, fp, _, _)| fp == &worker.2);
        left.push(worker);
        workers = right;

        stats.n_fingerprint += 1;
        stats.n_users += left.len();

        if left.len() == 1 {
            stats.n_unique += 1;
        }

        let ratio: f64 = left.iter()
            .fold(0., |acc, x| acc + x.4 );

        if size > 0 {
            stats.avg_ratio += ratio / stats.n_users as f64;
        }
    }

    if stats.n_fingerprint > 0 {
        let ffingerprint = stats.n_fingerprint as f64;
        stats.avg_users = stats.n_users as f64 / ffingerprint;
        stats.ratio_unique = stats.n_unique as f64 / ffingerprint;
        stats.avg_ratio /= ffingerprint;
    }

    stats
}

/// Add informations about the used tags (tags > 0) to the workers set
/// Return Vec<(user id, Vec<(tag id, ratio)>, tags above 0, avg ratios of all skills)>
fn anotate_group(workers: Vec<(i64, Vec<(u64, f64)>)>, size: usize) -> Vec<(i64, Vec<(u64, f64)>, String, f64)> {
    let fsize = size as f64;

    workers
        .into_par_iter()
        .map(|(u, skills)| {
            let group = freq_group(&skills);
            let avg = skills.iter().fold(0., |acc, (_, r)| acc + r);

            if size > 0 {
                (u, skills, group, avg / fsize)
            } else {
                (u, skills, group, 0.)
            }
        })
        .collect()
}

/// Get statistics per group of tags together
/// /// Return Vec<(user id, Vec<(tag id, ratio)>, fingerprint, tags above 0, avg ratios of all skills)>
fn stats_group_size_bis(workers: Vec<(i64, Vec<(u64, f64)>)>, size: usize, splits: usize) -> GroupsSizeStats_bis {
    let mut workers = anotate_group(workers, size);

    let mut stats = GroupsSizeStats_bis {
        gsize: size,
        n_users: 0,
        quartiles_nusers: vec![],
        n_group: 0,
        avg_ratio: 0., // NU
        quartiles_ratio: vec![], // NU
        avg_users_group: 0.,
        n_fingerprint: 0,
        avg_fingerprint: 0.,
        n_unique: 0,
        avg_unique: 0.,
        quartiles_unique: vec![]
    };

    while let Some(worker) = workers.pop() {
        // Extract workers with the same group of tags
        let (mut left, right): (Vec<_>, Vec<_>) = workers.into_par_iter().partition(|(_, _, g, _)| g == &worker.2);
        left.push(worker);
        workers = right;

        let stat_fps = stats_fingerprint(left, size, splits);

        stats.n_users += stat_fps.n_users;
        stats.quartiles_nusers.push(stats.n_users);
        stats.n_group += 1;
        stats.avg_ratio += stat_fps.avg_ratio;
        stats.quartiles_ratio.push(stat_fps.avg_ratio);
        stats.n_fingerprint += stat_fps.n_fingerprint;
        stats.n_unique += stat_fps.n_unique;
        stats.avg_unique += stat_fps.ratio_unique;
        stats.quartiles_unique.push(stat_fps.ratio_unique);
    }

    stats.avg_users_group = stats.n_users as f64 / stats.n_group as f64;
    stats.quartiles_nusers = quartiles_usize(stats.quartiles_nusers);
    stats.avg_ratio /= stats.n_group as f64;
    stats.quartiles_ratio = quartiles_f64(stats.quartiles_ratio);
    stats.avg_fingerprint = stats.n_fingerprint as f64 / stats.n_group as f64;
    stats.avg_unique /= stats.n_group as f64;
    stats.quartiles_unique = quartiles_f64(stats.quartiles_unique);

    stats
}



/// Get statistics per group size
fn stats_group_size(workers: Vec<(i64, Vec<(u64, f64)>)>, tags: &[u64], size: usize, splits: usize) -> GroupSizeStats{
    let fsize = size as f64;

    // map fingerprint to workers
    // Vec<(user id, Vec<(tag id, ratio)>, fingerprint, tags above 0, avg ratios of all skills)>
    let mut workers: Vec<(i64, Vec<(u64, f64)>, String, String, f64)> = workers
        .into_par_iter()
        .map(|(u, skills)| {
            let fingerprint = freq_group_fingerprint(&skills, splits);
            let group = freq_group(&skills);
            let avg = skills.iter().fold(0., |acc, (_, r)| acc + r);

            if size > 0 {
                (u, skills, fingerprint, group, avg / fsize)
            } else {
                (u, skills, fingerprint, group, 0.)
            }
        })
        .filter(|(_, _, f, _, _)| f != "")
        .collect();

    // number of users with this number of skills mastered
    let nusers = workers.len();
    // Agerage ratio of all users for this group size
    let mut ratio_avg = 0.;
    // totoal number of fingerprints
    let mut fingerprints = 0;
    // Average number of users per fingerprint
    let mut fingerprint_avg = 0usize;
    // Count of all fingerprint with only one user
    let mut unique_fingerprint = 0usize;
    // Keep information (avg, number of users) for each fingerprint for quartiles calculation
    let mut per_fingerprint: Vec<(f64, usize)> = vec![];

    // stats per fingerprint
    while let Some((_, skills, fingerprint, group, avg)) = workers.pop() {
        fingerprints += 1;

        // filter per fingerprint
        let (same, other): (Vec<_>, Vec<_>) = workers
            .into_par_iter()
            .partition(|(_, oskills, fp, _, _)| fp == &fingerprint);
        workers = other;

        // number of users with the same fingerprint
        let same_freq = same.len() + 1;
        fingerprint_avg += same_freq;

        if same_freq == 1 {
            unique_fingerprint += 1;
        }

        // Average ratio for this fingerprint
        let avg = avg + same.par_iter()
            .fold(|| 0., |acc, (_, _, _, _, a)| acc + a)
            .sum::<f64>();
        let avg_freq = avg / same_freq as f64;

        per_fingerprint.push((avg_freq, same_freq));

        // Add to the global stats
        ratio_avg += avg_freq;
    }

    if nusers > 0{
        ratio_avg /= nusers as f64;
    } else {
        ratio_avg = 0.;
    }

    let mut unique_ratio = 0.;
    if fingerprints > 0 {
        fingerprint_avg /= fingerprints;
        unique_ratio = unique_fingerprint as f64 / fingerprints as f64;
    } else {
        fingerprint_avg = 0;
    }

    let (q_avg, q_nusers) = calc_quartiles(per_fingerprint);

    GroupSizeStats {
        gsize: size,
        nusers: nusers,
        avg_ratio: ratio_avg,
        fingerprints: fingerprints,
        avg_fingerprints: fingerprint_avg,
        unique_fingerprints: unique_fingerprint,
        unique_ratio: unique_ratio,
        quartiles_ratio: q_avg,
        quartiles_nusers: q_nusers
    }
}


/// Calculate metadata on groups of skills
pub fn skills_meta(workers: WorkersSet, tags: &[u64], splits: usize) -> (Vec<GroupsSizeStats_bis>, usize){
    let mut res = vec![];
    let mut workers = workers.0;

    let mut n_unique = 0;

    for i in 0..tags.len() {
        println!("Size {}", i);

        let (left, right): (Vec<_>, Vec<_>) = workers.into_par_iter().partition(|(u, skills)| group_size(&skills) == i);
        workers = right;

        let stat = stats_group_size_bis(left, /*tags,*/i, splits);
        n_unique += stat.n_unique;
        res.push(stat);
    }

    (res, n_unique)
}