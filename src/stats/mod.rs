mod misc;
mod tags;
mod group_anon_size;
mod skills_meta;
mod skills;

use std::{thread, time};

use std::{
    fs::File,
    error::Error,
    collections::HashMap
};
use rayon::prelude::*;
use serde::Serialize;
use rayon::iter::split;

use crate::{
    generation::{RawWorkersSet, WorkersSet},
    stats::{
        misc::split_index
    }
};
use crate::stats::tags::TagsStats;
use crate::stats::skills::{NumberOfSkillsStats, GroupsStats};
use crate::stats::skills_meta::{GroupSizeStats, GroupsSizeStats_bis};


/// Get the average skill level for both (zero and non zero).
fn stats_avg_skills(ratios: &RawWorkersSet) -> (f64, f64) {
    let zero_len = ratios.0.len() as f64;
    let zero_sum: f64 = ratios.0.par_iter().map(|(_, _, x)| x).sum();

    let nz_len = ratios.0.par_iter()
        .filter(|(_, _, x)| *x > 0.)
        .map(|(_, _, x)| x)
        .count() as f64;
    let nz_sum: f64 = ratios.0.par_iter()
        .filter(|(_, _, x)| *x > 0.)
        .map(|(_, _, x)| x)
        .sum();

    (zero_sum / zero_len, nz_sum / nz_len)
}

/// Calculate the anonymity group per group of skills
/// Return a Vec<(number of users, number of groups)> sorted by inc number of users
fn stats_groups_anon(groups: &Vec<GroupsStats>) -> Vec<(usize, usize)> {
    let mut stats: HashMap<usize, usize> = HashMap::new();

    for x in groups{
        *stats.entry(x.nusers).or_insert(0) += 1;
    }

    let mut stats: Vec<(usize, usize)> = stats.drain().collect();
    stats.par_sort_by(|(a, _),(b, _)| a.cmp(b));
    stats
}

/// Statistics on a generated dataset
#[derive(Debug, Serialize)]
pub struct UvStats {
    /// Used tags
    tags: Vec<u64>,
    /// Splits (number of elements in frequency (range = 1 / splits)
    splits: usize,
    /// Total number of users
    nusers: usize,
    /// Number (total) of unique users
    n_unique: usize,
    /// Ratio n_unique / nusers of unique users
    ratio_unique: f64,
    /// Per group of n skills mastered
    per_groups: Vec<GroupsSizeStats_bis>,
    // /// Stats per number of skills (> 0) per users : (number of users, avg ratio (> 0), avg ratio)
    // per_n_skills: Vec<NumberOfSkillsStats>,
    // /// Stats per group of skills (> 0) per users : (skills, number of skills, number of users, avg ratio (> 0), avg ratio)
    // per_skills_grouped: Vec<GroupsStats>,
    // /// Number of groups per number of users in it (number of users / groups, number of groups)
    // per_groups_anon: Vec<(usize, usize)>,
    // /// Per groups size on the unicity : number of users, unicity ratio (and min/max), number of unique users
    // per_groups_size: Vec<group_anon_size::GroupAnonSizeStats>,
    // /// Average number of skills
    // avg_spe: f64,
    // /// Average number of skills (> 0)
    // avg_spe_nz: f64,
    /// Average skill level
    avg: f64,
    /// Average skill level (> 0)
    avg_nz: f64,
    /// Statistics per tags (tag id, average ratio, minimum ratio, maximum ratio, number of users (> 0), frequency)
    per_tags: Vec<tags::TagsStats>
}

impl UvStats {

    /// Create statistics from raw data
    pub fn new(mut ratios: RawWorkersSet, tags: Vec<u64>, splits: usize) -> Self {
        let stats = tags::stats_tags_bis(&mut ratios, &tags, splits);

        let (avg_zero, avg_nz) = stats_avg_skills(&ratios);
        // stats on WorkersSet
        let ratios_grouped = ratios.as_workersset();
        let nusers = ratios_grouped.0.len();

        let (sm, n_unique) = skills_meta::skills_meta(ratios_grouped, &tags, splits);

        // let (nskills, groups, avg_spe, avg_spe_nz) = skills::stats_skills(&ratios_grouped, &tags, splits);
        // let groups_anon = stats_groups_anon(&groups);
        // let groups_size = group_anon_size::stats_groups_anon_size(&groups);


        Self {
            tags: tags,
            splits: splits,
            nusers: nusers,
            n_unique: n_unique,
            ratio_unique: n_unique as f64 / nusers as f64,
            per_groups: sm,
            avg: avg_zero,
            avg_nz: avg_nz,
            per_tags: stats
        }
    }

    /// Print (on standard output) the
    pub fn print(&self) {
        println!("{:?}", self);
    }

    /// Write the results in a json file
    pub fn write_json(&self, path: &str) -> Result<(), Box<Error>>{
        let file = File::create(path)?;
        serde_json::to_writer_pretty(file, &self)?;
        Ok(())
    }
}

/// Calculate statistics on a workers set
pub fn statistics(workers: &str, splits: usize, tags: &[u64], json: Option<String>) -> Result<UvStats, Box<Error>>{
    let ratios = RawWorkersSet::load_csv(workers)?;
    println!("Loaded");

    let stats = UvStats::new(ratios, tags.iter().cloned().collect(), splits);
    stats.print();

    match json {
        Some(path) => stats.write_json(&path)?,
        None => {}
    };

    Ok(stats)
}