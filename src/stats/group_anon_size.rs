use std::collections::HashMap;
use rayon::prelude::*;
use serde::Serialize;

use crate::stats::skills::GroupsStats;

#[derive(Debug, Serialize)]
pub struct GroupAnonSizeStats {
    /// number of tags in the group
    ntags: usize,
    /// number of users concerned
    nusers: usize,
    /// average ratio of unique frequency
    avg_ratio: f64,
    /// min ratio of unique frequency
    min_ratio: f64,
    /// max ratio of unique frequency
    max_ratio: f64,
    /// number of unique users
    users_unique: usize
}

struct GroupAnon {
    nusers: usize,
    avg_ratio: f64,
    min_ratio: f64,
    max_ratio: f64,
    users_unique: usize
}

/// Calculate statistics on the groups size and the anonymity groups
pub fn stats_groups_anon_size(groups: &Vec<GroupsStats>) -> Vec<GroupAnonSizeStats> {
    let mut stats: HashMap<usize, GroupAnon> = HashMap::new();

    // (tags, _, n, _, _, _, r, nu)
    for x in groups {
        let tl = x.size;

        let stat = stats.entry(tl).or_insert( GroupAnon {
            nusers: 0,
            avg_ratio: 0.,
            min_ratio: 0.,
            max_ratio: 0.,
            users_unique: 0
        });
        stat.nusers += x.nusers;
        stat.avg_ratio += x.unique_ratio;

        if x.unique_ratio < stat.min_ratio {
            stat.min_ratio = x.unique_ratio;
        }

        if x.unique_ratio > stat.max_ratio {
            stat.max_ratio = x.unique_ratio;
        }

        stat.users_unique += x.unique_number;

    }

    let mut stats: Vec<GroupAnonSizeStats> = stats.drain().map(|(k, v)|
        GroupAnonSizeStats{
            ntags: k,
            nusers: v.nusers,
            avg_ratio: v.avg_ratio / v.nusers as f64,
            min_ratio: v.min_ratio,
            max_ratio: v.max_ratio,
            users_unique: v.users_unique
        }
    ).collect();
    stats.par_sort_by(|a, b| a.ntags.cmp(&b.ntags));
    stats
}