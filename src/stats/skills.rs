use std::collections::HashMap;
use rayon::prelude::*;
use serde::Serialize;

use crate::{
    generation::{RawWorkersSet, WorkersSet},
    stats::misc::split_index
};

/// Stats on the number of skills mastered
#[derive(Debug, Serialize, Clone)]
pub struct NumberOfSkillsStats {
    /// number of users mastering this number of skills
    number: usize,
    /// average ratio (above 0)
    avg_ratio_nz: f64,
    /// average ratio
    avg_ratio: f64
}

/// Groups of tags often together
#[derive(Debug, Serialize)]
pub struct GroupsStats {
    /// Vec<tag id>
    pub tags: Vec<u64>,
    /// group size (number of tags)
    pub size: usize,
    /// Number of users
    pub nusers: usize,
    /// Average ration (> 0)
    pub avg_ratio_nz: f64,
    /// Average ratio
    pub avg_ratio: f64,
    /// Frequency per tags, ordered by tag
    /// Vec<(tag id, Vec<frequency>)>
    pub frequency: Vec<(u64, Vec<f64>)>,
    /// ration of unique frequency
    pub unique_ratio: f64,
    /// Number of unique frequency
    pub unique_number: usize
}

struct GroupTag {
    /// Number of users
    nusers: usize,
    /// Average ration (> 0)
    avg_ratio_nz: f64,
    /// Average ratio
    avg_ratio: f64,
    /// Frequency per tags, ordered by tag
    /// Vec<(tag id, Vec<frequency>)>
    frequency: HashMap<u64, Vec<usize>>,
    /// Frequency unicity
    unicity: HashMap<String, usize>
}


/// Calculate statistics on the users tags usage
/// Return (Vec<Stats on the number of skills mastered>,
///         Vec<(   Groups of tags often together
///             Vec<tag id>,
///             group size,
///             number of users,
///             Vec<(    frequency per tags, ordered by tag
///                 tag id,
///                 Vec<frequency>
///             )>,
///             ratio of unique frequency,
///             number of unique frequency
///         )>,
///         average number of tags,
///         average number of tags above 0
/// )
pub fn stats_skills(ratios: &WorkersSet, tags: &[u64], splits: usize)
                -> (Vec<NumberOfSkillsStats>, Vec<GroupsStats>, f64, f64) {
    let nusers = ratios.0.len() as f64;
    let ntags = tags.len();

    let mut nskills = vec![NumberOfSkillsStats{
        number: 0,
        avg_ratio_nz: 0.,
        avg_ratio: 0.
    } ; ntags + 1];
    //let mut groups: HashMap<Vec<u64>, (usize, f64, f64, HashMap<u64, Vec<usize>>, HashMap<String, usize>)> = HashMap::new();
    let mut groups: HashMap<Vec<u64>, GroupTag> = HashMap::new();

    let mut zero = 0;
    let mut avg = 0.;
    let mut avg_nz = 0.;

    for (_, v) in &ratios.0 {
        // get only the tags / ratios above zero
        let nz_ratios: Vec<(u64, f64)> = v.iter()
            .filter(|(t, r)| r != &0.)
            .cloned()
            .collect();

        let nz_tags: Vec<u64> = nz_ratios.iter().map(|(t, _)| *t).collect();

        let c = nz_ratios.len();
        let sum_ratios: f64 = nz_ratios.iter().map(|(_, r)| *r).sum();

        let avg_tags_nz = sum_ratios / c as f64;
        let avg_tags_z = sum_ratios / ntags as f64;

        nskills[c].number += 1;
        nskills[c].avg_ratio_nz += avg_tags_nz;
        nskills[c].avg_ratio += avg_tags_z;

        let group_entry = groups.entry(nz_tags).or_insert(GroupTag {
            nusers: 0,
            avg_ratio_nz: 0.,
            avg_ratio: 0.,
            frequency: HashMap::new(),
            unicity: HashMap::new()
        });
        group_entry.nusers += 1;
        group_entry.avg_ratio_nz += avg_tags_nz;
        group_entry.avg_ratio += avg_tags_z;

        if c == 0 {
            zero += 1;
        }

        if c > 0 {
            avg_nz += c as f64;
        }

        avg += c as f64;

        // get frequency
        let mut fingerprint = "".to_string();

        for (tag, ratio) in v {
            let freq = group_entry.frequency.entry(*tag).or_insert(vec![0; splits]);
            let si = split_index(*ratio, splits);
            freq[si] += 1;
            fingerprint += &format!("{}-{} ", tag, si);
        }
        let fps = group_entry.unicity.entry(fingerprint).or_insert(0);
        *fps += 1;
    }

    nskills = nskills.drain(..).map(|mut x| {
        let n = x.number as f64;
        x.avg_ratio_nz /= n;
        x.avg_ratio /= n;
        x
    }).collect();

    let mut groups: Vec<GroupsStats> = groups.drain()
        .map(|(k, mut g)| {
            let len = k.len();
            let fc = g.nusers as f64;
            let nunique = g.unicity.iter().filter(|(_, n)| *n == &1).count();

            GroupsStats {
                tags: k,
                size: len,
                nusers: g.nusers,
                avg_ratio_nz: g.avg_ratio_nz / fc,
                avg_ratio: g.avg_ratio / fc,
                frequency: {
                    // Add absent tags
                    for tag in tags {
                        g.frequency.entry(*tag).or_insert(vec![0; splits]);
                    }

                    g.frequency.drain().map(|(k, mut v)| (k,
                                                   v.drain(..).map(|x| x as f64 / fc).collect()
                    )).collect()
                },
                unique_ratio: nunique as f64 / fc,
                unique_number: nunique
            }
        }).collect();
    groups.par_sort_by(|a, b| a.size.cmp(&b.size));

    for group in &mut groups {
        group.frequency.par_sort_by(|(a, _), (b, _)| a.cmp(b));
    }

    (nskills, groups, avg / nusers , avg_nz / (nusers - zero as f64))
}