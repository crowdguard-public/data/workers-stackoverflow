import mysql.connector
from lxml.etree import iterparse, tostring

# Tags
# path = './Tags.xml'
# stmt = "insert into Tags (Id, TagName, Count) values (%s, %s, %s)"
# wc = 54000

# Users
# path = './Users.xml'
# stmt = "insert into Users (Id, Reputation, DisplayName, Location, Views, UpVotes, DownVotes) values (%s, %s, %s, %s, %s, %s, %s)"
# wc = 10097980

# Posts
# path = './Posts.xml'
# stmt = "insert into Posts (Id, PostTypeId, AcceptedAnswerId, ParentId, Score, ViewCount, OwnerUserId, Tags, AnswerCount, CommentCount, FavoriteCount) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
# wc = 43872994

# Votes
path = './xah'
stmt = "insert into Votes (Id, PostId, VoteTypeId) values (%s, %s, %s)"
wc = 20000000

conn = mysql.connector.connect(host='localhost', user='root', password='pwd', database='stackoverflow')
cursor = conn.cursor(prepared=True)

context = iterparse(path, events=('end',), tag='row')
i = 0
pc = 0

s_tags = {}

for event, elem in context:
    try:
        vals = (
            elem.get('Id'),
            elem.get('PostId'),
            elem.get('VoteTypeId')
        )

        # print(vals)
        r = cursor.execute(stmt, vals)
    except Exception as e:
        se = str(e)

        if not ("Duplicate entry" in se) or "":
            print(e)
            exit(-1)

    i += 1
    if i % 1000 == 0:
        npc = round((i * 100) / wc, 2)
        if npc != pc:
            pc = npc
            print(i, '/', wc, ':', pc, "%")

        conn.commit()

    elem.clear()
    while elem.getprevious() is not None:
        del elem.getparent()[0]

cursor.close()
conn.close()
