import mysql.connector
import csv
from lxml.etree import iterparse, tostring

# Tags
path = './Tags.xml'
out = './Tags.csv'
wc = 54000

context = iterparse(path, events=('end',), tag='row')
csvfile = open(out, "w")
csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

for event, elem in context:
    id = elem.get('Id')
    tagname = elem.get('TagName')

    csvwriter.writerow([id, tagname])

    elem.clear()
    while elem.getprevious() is not None:
        del elem.getparent()[0]

csvfile.close()