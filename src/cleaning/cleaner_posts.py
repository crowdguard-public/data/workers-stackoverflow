import mysql.connector
from lxml.etree import iterparse, tostring
import csv

# Load tags in RAM
path = './Tags.csv'
s_tags = {}

with open(path, 'r') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',')

    for row in csvreader:
        id = int(row[0])
        tagname = row[1]
        s_tags[tagname] = id

print("Tags loaded ...")

# Posts
# Split : 5000000
path = './xae'
out_posts = './Posts_xai.csv'
out_tags = './PostTags_xai.csv'
wc = 5000000

csv_posts = open(out_posts, 'w')
csvw_posts = csv.writer(csv_posts, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

csv_tags = open(out_tags, 'w')
csvw_tags = csv.writer(csv_tags, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

context = iterparse(path, events=('end',), tag='row')
i = 0
pc = 0

for event, elem in context:
    vals = [
        elem.get('Id'),
        elem.get('PostTypeId'),
        elem.get('AcceptedAnswerId'),
        elem.get('ParentId'),
        elem.get('Score'),
        elem.get('ViewCount'),
        elem.get('OwnerUserId'),
        elem.get('Tags'),
        elem.get('AnswerCount'),
        elem.get('CommentCount'),
        elem.get('FavoriteCount'),
    ]

    if vals[6] is None:
        continue

    # print(vals)

    if vals[7] is not None:
        postid = vals[0]
        tags = vals[7].split('>')
        for tag in tags:
            if len(tag) > 0:
                tag = tag[1:]

                tagid = s_tags.get(tag)
                if tagid is not None:
                    csvw_tags.writerow([postid, tagid])

    csvw_posts.writerow(vals)


    i += 1
    if i % 100 == 0:
        npc = round((i * 100) / wc, 2)
        if npc != pc:
            pc = npc
            print(i, '/', wc, ':', pc, "%")

    elem.clear()
    while elem.getprevious() is not None:
        del elem.getparent()[0]

csvfile.close()
