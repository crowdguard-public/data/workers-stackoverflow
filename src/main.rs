use std::fs::File;
use std::io::prelude::*;
use rayon::prelude::*;
use clap::{App, SubCommand, Arg, ArgMatches};
use rayon::iter::split;

/// Votes reduction code
mod pcvpp;
/// Tags and PostTags management
mod tags;
/// Users generation methods
mod generation;
/// Statistics on he user generation
mod stats;

/// Get the CLI arguments
fn cli_args() -> ArgMatches<'static>{
    App::new("Stack overflow analysis tool")
        .subcommand(SubCommand::with_name("redvote")
            .about("Reduce de Votes table and add 3 rows to each posts")
            .arg(Arg::with_name("votes")
                .short("v")
                .long("votes")
                .takes_value(true)
                .multiple(true)
                .required(true)
                .help("Votes CSV files paths")
            )
            .arg(Arg::with_name("posts")
                .short("p")
                .long("posts")
                .takes_value(true)
                .required(true)
                .help("Posts CSV file path")
            )
            .arg(Arg::with_name("out")
                .short("o")
                .long("out")
                .takes_value(true)
                .required(true)
                .help("New Posts CSV file path")
            )
        )
        .subcommand(SubCommand::with_name("merge")
            .about("Merge the generated workers set with the same users but with different tags in a single workers set file.")
            .arg(Arg::with_name("files")
                .long("files")
                .short("f")
                .takes_value(true)
                .multiple(true)
                .required(true)
                .help("Workers set files. All files must have the same users ordered by id.")
            )
            .arg(Arg::with_name("out")
                .long("out")
                .short("o")
                .takes_value(true)
                .required(true)
                .help("Output workers set file.")
            )
        )
        .subcommand(SubCommand::with_name("gen")
            .about("Generate a workers set and get statistics on this set")
            .arg(Arg::with_name("posttags")
                .short("r")
                .long("posttags")
                .takes_value(true)
                .required_unless("nogen")
                .help("PostTags relation CSV file")
            )
            .arg(Arg::with_name("posts")
                .short("p")
                .long("posts")
                .takes_value(true)
                .required_unless("nogen")
                .help("Posts (with reduced votes) CSV file path.")
            )
            .arg(Arg::with_name("workers")
                .short("w")
                .long("workers")
                .takes_value(true)
                .required(true)
                .help("Generated users CSV file path (generation output, statistics input).")
            )
            .arg(Arg::with_name("tags")
                .short("t")
                .long("tags")
                .takes_value(true)
                .multiple(true)
                .required_unless("tagsfile")
                .help("Filtering by selected tags")
            )
            .arg(Arg::with_name("tagsfile")
                .long("tagsfile")
                .takes_value(true)
                .required_unless("tags")
                .help("File containing the tags to filter with (one tag per line)")
            )
            .arg( Arg::with_name("nostats")
                .long("nostats")
                .conflicts_with("nogen")
                .help("Don't generate statistics")
            )
            .arg(Arg::with_name("nogen")
                .long("nogen")
                .conflicts_with("nostats")
                .help("Don't generate the dataset (only statistics on an existing dataset")
            )
            .arg(Arg::with_name("json")
                .long("json")
                .takes_value(true)
                .help("Write the statistics in a json file (ignored in nostats mode).")
            )
            .arg(Arg::with_name("splits")
                .long("splits")
                .takes_value(true)
                .default_value("10")
                .help("Number of separation for the frequency analysis (10 by default).")
            )
        )
        .get_matches()
}

/// Handle the call of pcvpp in the case of the redvote subcommand
fn hndl_redvote(args: &ArgMatches) {
    let votes = args.values_of_lossy("votes").unwrap();
    let rvotes: Vec<&str> = votes.iter().map(|s| s.as_ref()).collect();

    let posts = args.value_of_lossy("posts").unwrap();
    let out = args.value_of_lossy("out").unwrap();

    pcvpp::pcvpp(&rvotes,
                 &posts, &out);
}

/// Get tags list from args
fn get_tags(args: &ArgMatches) -> Vec<u64> {
    let stags = if args.is_present("tags") {
        args.values_of_lossy("tags").unwrap()
    } else {
        let path = args.value_of_lossy("tagsfile").unwrap().to_string();
        let mut file = File::open(path).unwrap();
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        contents.lines().map(|x| x.to_string()).collect()
    };

    let mut itags: Vec<u64> = vec![];
    for stag in stags {
        match u64::from_str_radix(&stag, 10) {
            Ok(itag) => {
                itags.push(itag);
            },
            Err(err) => {
                panic!("Tag is not a number : '{}'", &stag);
            }
        }
    }

    itags
}

/// Handle the call to the workers generation
fn hndl_gen(args: &ArgMatches) {
    let workers = args.value_of_lossy("workers").unwrap();

    let itags = get_tags(args);

    if ! args.is_present("nogen") {
        let posttags = args.value_of_lossy("posttags").unwrap();
        let posts = args.value_of_lossy("posts").unwrap();

        generation::ratiouv::ratiouv(&posttags, &posts, &workers, &itags).unwrap();
    }

    if ! args.is_present("nostats") {
        let json = match args.value_of_lossy("json") {
            Some(p) => Some(p.to_string()),
            None => None
        };

        let splits = usize::from_str_radix(&args.value_of_lossy("splits").unwrap(), 10).unwrap();

        stats::statistics(&workers, splits, &itags, json).unwrap();
    }
}

fn hndl_merge(args: &ArgMatches) {
    let files = args.values_of_lossy("files").unwrap();
    let out = args.value_of("out").unwrap();

    generation::merge_workers_files(&files, &out).unwrap();
}


fn main() {
    let args = cli_args();

    if let Some(redvote) = args.subcommand_matches("redvote") {
        hndl_redvote(redvote);
    }

    if let Some(gen) = args.subcommand_matches("gen") {
        hndl_gen(gen);
    }

    if let Some(merge) = args.subcommand_matches("merge") {
        hndl_merge(merge);
    }

    //ratiouv::launch_ratiouv();
}