use std::error::Error;
use rayon::prelude::*;
use std::cmp::Ordering;

/// Load the PostTags CSV file to memory
/// Result as a Vec<(postid, tagid)> sorted by postid asc then by tagid asc for equals postid
pub fn load_posttags_file(path: &str) -> Result<Vec<(u64, u64)>, Box<Error>> {
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(path)?;

    let mut v: Vec<(u64, u64)> = vec![];

    for record in reader.records(){
        let record = record?;

        let postid = u64::from_str_radix(record.get(0).unwrap(), 10)?;
        let tagid = u64::from_str_radix(record.get(1).unwrap(), 10)?;

        v.push((postid, tagid));
    }

    v.sort_by(|(a_p, a_t), (b_p, b_t)| {
        if a_p < b_p {
            Ordering::Less
        } else if a_p > b_p {
            Ordering::Greater
        } else {
            if a_t < b_t {
                Ordering::Less
            } else if a_t > b_t {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        }
    });

    Ok(v)
}

/// Filter posttags to limit by given tags
pub fn posttags_filter(posttags: Vec<(u64, u64)>, tags: &[u64]) -> Vec<(u64, u64)> {
    posttags.par_iter()
        .filter(|(p, t)| match tags.iter().position(|x| x == t) {
            Some(_) => true,
            None => false
        })
        .cloned()
        .collect()
}