use std::fs::File;
use std::error::Error;
use rayon::prelude::*;
use std::cmp::Ordering;


/// Load the CSV file in memory
/// Vec<(postid, votetype)>
fn load_file(file: &str) -> Result<Vec<(u64, u64)>, Box<Error>>{
    let file = File::open(file)?;
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(file);

    let mut v: Vec<(u64, u64)> = vec![];

    for record in reader.records(){
        let record = record?;

        let postid = u64::from_str_radix(record.get(1).unwrap(), 10)?;
        let votetype = u64::from_str_radix(record.get(2).unwrap(), 10)?;

        v.push((postid, votetype));
    }

    // sorting
    v.par_sort_by(|(a_p, a_t), (b_p, b_t)| {
        if a_p < b_p {
            Ordering::Less
        } else if a_p > b_p {
            Ordering::Greater
        } else {
            if a_t < b_t {
                Ordering::Less
            } else if a_t > b_t {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        }
    });


    Ok(v)
}

/// Aggregate the lines of the csv (as a Vec<postid, votetype)> to a smaller list
/// Vec<(postid, votetype, count)>
fn sum_votes(votes: Vec<(u64, u64)>) -> Vec<(u64, u64, usize)> {
    let mut res: Vec<(u64, u64, usize)> = vec![];
    let mut postid = votes[0].0;
    let mut votetype = votes[0].1;
    let mut count = 1;

    for i in 1..votes.len() {
        let (p, v) = votes[i];

        if p != postid || v != votetype{
            res.push((postid, votetype, count));

            postid = p;
            votetype = v;
            count = 1;
        } else {
            count += 1;
        }
    }

    res
}

/// Aggregate a list of Vec<(postid, votetype, count)> to a single Vec<(postid, votetype, count)>
fn agregate_sums(mut sums: Vec<Vec<(u64, u64, usize)>>) -> Vec<(u64, u64, usize)> {
    let mut votes: Vec<(u64, u64, usize)> = vec![];

    // grouping
    for sum in sums.drain(..) {
        votes = votes.par_iter().chain(sum.par_iter()).cloned().collect();
    }

    // sorting
    votes.par_sort_by(|(a_p, a_t, _), (b_p, b_t, _)| {
        if a_p < b_p {
            Ordering::Less
        } else if a_p > b_p {
            Ordering::Greater
        } else {
            if a_t < b_t {
                Ordering::Less
            } else if a_t > b_t {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        }
    });

    // reducing
    let mut res: Vec<(u64, u64, usize)> = vec![];
    let mut postid = votes[0].0;
    let mut votetype = votes[0].1;
    let mut count = 1;

    for i in 1..votes.len() {
        let (p, v, c) = votes[i];

        if p != postid || v != votetype{
            res.push((postid, votetype, count));

            postid = p;
            votetype = v;
            count = c;
        } else {
            count += c;
        }
    }

    res
}

fn add_to_posts(votes: Vec<(u64, u64, usize)>, posts: &str, out: &str) -> Result<(), Box<Error>>{
    let fin = File::open(posts)?;
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(fin);

    let mut writer = csv::Writer::from_path(out)?;

    let mut ivotes = 0;

    for record in reader.records() {
        let record = record?;
        let postid = u64::from_str_radix(record.get(0).unwrap(), 10)?;

        while votes[ivotes].0 < postid && ivotes < votes.len(){
            ivotes += 1;
        }


        let mut pv: Vec<String> = record.iter().map(|s| s.to_string()).collect();

        for t in 1..4 {
            if votes[ivotes].0 == postid {
                if votes[ivotes].1 == t {
                    pv.push(votes[ivotes].2.to_string());
                    ivotes += 1;
                } else {
                    pv.push("0".to_string());
                }
            } else {
                pv.push("0".to_string());
            }
        }

        writer.write_record(pv)?;
    }

    Ok(())
}


/// Reducing the Votes table to add 3 columns at the end of each posts (best answer, up votes, down votes)
pub fn pcvpp(votes_ins: &[&str], posts: &str, out: &str) {
    let sums = votes_ins.par_iter()
        .map(|file| {
            let votes = load_file(file).unwrap();
            sum_votes(votes)
        }).collect();
    println!("Summed");

    let res = agregate_sums(sums);

    println!("Aggregated");

    add_to_posts(res, posts, out).unwrap();
}

pub fn launch_pcvpp() {
    let votes_files = [
        "../votes/xaa",
        "../votes/xab",
        "../votes/xac",
        "../votes/xad",
        "../votes/xae",
        "../votes/xaf",
        "../votes/xag",
        "../votes/xah",
    ];

    pcvpp(&votes_files, "../posts/stackoverflow_Posts.csv", "newPosts.csv");
}