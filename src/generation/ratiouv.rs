use std::error::Error;
use rayon::prelude::*;
use std::collections::HashMap;
use std::cmp::Ordering;

use crate::tags;
use crate::generation::WorkersSet;

/// A stackoverflow post
struct Post {
    /// Post id or post parent id
    postid: u64,
    /// owner id
    userid: i64,
    /// Post ratio
    ratio: f64
}

/// Load posts from file and calculate the ratio.
///
/// ratio = (Up votes / (Up votes / Down votes))
fn load_posts(posts_path: &str) -> Result<Vec<Post>, Box<Error>> {
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(posts_path)?;

    let mut posts = vec![];

    for record in reader.records() {
        let record = record?;

        let postid = u64::from_str_radix(record.get(0).unwrap(), 10)?;
        let parentid = record.get(3).unwrap();
        let postid = if parentid.len() > 0 {
            u64::from_str_radix(parentid, 10)?
        } else {
            postid
        };

        let owner = i64::from_str_radix(record.get(6).unwrap(), 10)?;
        // let best = u64::from_str_radix(record.get(11).unwrap(), 10)?;
        let up = u64::from_str_radix(record.get(12).unwrap(), 10)?;
        let down = u64::from_str_radix(record.get(13).unwrap(), 10)?;

        if up + down < 10 { // ignore posts with less than 10 votes
            let ratio = if up + down == 0 {
                0.
            } else {
                up as f64 / (up as f64 + down as f64)
            };

            posts.push(Post{
                postid: postid,
                userid: owner,
                ratio: ratio
            });
        }
    }

    posts.par_sort_by(|a, b| a.postid.cmp(&b.postid));

    Ok(posts)
}

/// Get the ratio per tags per posts.
/// The PostId, useless after this function is now removed.
///
/// Return Vec<(owneruserid, Vec<(tag, ratio)>). Each rows of the Vec corresponding to a post.
fn posts_ratio_uv(posts: Vec<Post>, posttags: &[(u64, u64)]) -> Vec<(i64, Vec<(u64, f64)>)>{
    let mut ratios = vec![];
    let mut itags = 0;
    let mut itags_old = 0;

    // for each posts
    'l : for post in posts {
        // get the tags used in the post
        let mut used_tags = vec![];

        if itags < posttags.len() {
            while posttags[itags].0 < post.postid {
                itags += 1;

                if itags >= posttags.len() {
                    continue 'l;
                }
            }

            itags_old = itags;

            while posttags[itags].0 == post.postid {
                // link the used tag to the post ratio
                used_tags.push((posttags[itags].1, post.ratio));

                itags += 1;
                if itags >= posttags.len() {
                    break;
                }
            }
        }

        itags = itags_old;

        if ! used_tags.is_empty() {
            ratios.push((post.userid, used_tags));
        }
    }

    ratios
}

/// Calculate a ratio per users and per tags
///
/// * `post_ratio` - Vec<(owneruserid, Vec<(tag, ratio)>). Each rows of the Vec corresponding to a post.
/// * `tags` - List of tags used
///
/// Return Vec<(user id, Vec<tag id, ratio)>)>
fn users_ratio_uv(mut posts_ratio: Vec<(i64, Vec<(u64, f64)>)>, tags: &[u64]) -> WorkersSet{
    posts_ratio.par_sort_by(|(a, _), (b, _)| a.cmp(b) );

    let mut ratios = vec![];

    let mut user = 0;
    let mut utags: HashMap<u64,(f64, u64)> = HashMap::new();

    for (userid, ratiotags) in posts_ratio {
        if userid != user{
            if ! utags.is_empty() && user != 0 {
                let rtags: Vec<(u64, f64)> = utags.drain()
                    .map(|(k, (r, n))| (
                        k,
                        if n == 0 {
                            0.
                        } else {
                            r / n as f64
                        }
                    ))
                    .collect();
                ratios.push((user, rtags));
            }

            user = userid;
            utags = HashMap::new();

            for t in tags {
                utags.insert(*t, (0., 0));
            }

        }

        for (tag, ratio) in ratiotags {
            let (r, n) = utags.entry(tag).or_insert((0., 0));
            *r += ratio;
            *n += 1;
        }
    }

    // filter workers with zero skills mastered
    ratios = ratios.drain(..).filter(|(u, v)| ! v.iter().all(|(t, r)| r == &0.)).collect();

    WorkersSet(ratios)
}

/// Generate the ratiouv workers dataset and save the workers set in a CSV file
///
/// * `posttags` - PostTag CSV file path
/// * `post_path` - Posts CSF file path
/// * `path_workers` - Generated workers CSV file
/// * `tags` - List of tags (dimensions in the generated dataset)
///
/// Return : Vec<(user id, Vec<(tag id, ratio)>)>
pub fn ratiouv(posttags: &str, posts_path: &str, path_workers: &str, tags: &[u64]) -> Result<WorkersSet, Box<Error>>{
    let posttags = tags::load_posttags_file(posttags)?;
    println!("PostTags loaded");

    let posttags = tags::posttags_filter(posttags, tags);
    println!("PostTags filtered");

    let posts = load_posts(posts_path)?;
    println!("Posts loaded, ratio calculated");

    let ratios = posts_ratio_uv(posts, &posttags);

    println!("Ratios / Posts / tags calculated");

    let ratios = users_ratio_uv(ratios, tags);

    println!("Workers generated");

    ratios.write_csv(path_workers)?;

    println!("Workers saved in the CSV file");

    Ok(ratios)
}