pub mod ratiouv;

use std::error::Error;
use std::io::{self, ErrorKind};
use std::str::FromStr;
use std::cmp::Ordering;
use rayon::prelude::*;

/// Raw set of workers (user id, tag id, ratio)
pub struct RawWorkersSet(pub Vec<(i64, u64, f64)>);

impl RawWorkersSet {

    /// Load a CSV file containing a workers set
    pub fn load_csv(path: &str) -> Result<Self, Box<Error>> {
        // create the CSV reader
        let mut reader = csv::ReaderBuilder::new()
            .has_headers(false)
            .from_path(path)?;

        let mut ratios = vec![];

        for record in reader.records() {
            let record = record?;

            let userid = i64::from_str_radix(record.get(0).unwrap(), 10)?;
            let tagid = u64::from_str(record.get(1).unwrap())?;
            let ratio = f64::from_str(record.get(2).unwrap())?;

            ratios.push((userid, tagid, ratio));
        }

        Ok(Self(ratios))
    }

    /// Write the ratios in a CSV file
    /// Each rows = (user id, tag id, ratio)
    pub fn write_csv(&self, path: &str) -> Result<(), Box<Error>> {
        let mut writer = csv::Writer::from_path(path)?;

        for (u, t, r) in &self.0 {
            writer.write_record(&[u.to_string(), t.to_string(), r.to_string()])?;
        }

        Ok(())
    }

    /// Transform a raw worker set to a workers set by users
    pub fn as_workersset(mut self) -> WorkersSet {
        self.0.par_sort_by(|(a_u, a_t, _), (b_u, b_t, _)|
            if a_u < b_u {
                Ordering::Less
            } else if a_u > b_u {
                Ordering::Greater
            } else {
                if a_t < b_t {
                    Ordering::Less
                } else if a_t > b_t {
                    Ordering::Greater
                } else {
                    Ordering::Equal
                }
            }
        );

        let mut ratios = vec![];

        let mut user = 0;
        let mut values = vec![];

        for (u, t, r) in self.0.drain(..) {
            if u == user {
                values.push((t, r));
            } else {
                if user != 0 {
                    values.par_sort_by(|(a, _), (b, _)| a.cmp(b));
                    ratios.push((user, values.drain(..).collect()));
                }

                user = u;
            }
        }

        if user != 0 {
            values.par_sort_by(|(a, _), (b, _)| a.cmp(b));
            ratios.push((user, values.drain(..).collect()));
        }

        WorkersSet(ratios)
    }
}

pub struct WorkersSet(pub Vec<(i64, Vec<(u64, f64)>)>);

impl WorkersSet {

    /// Load a CSV file containing a workers set
    pub fn load_csv(path: &str) -> Result<Self, Box<Error>> {
        Ok(RawWorkersSet::load_csv(path)?.as_workersset())
    }


    /// Write the ratios in a CSV file
    /// Each rows = (user id, tag id, ratio)
    fn write_csv(&self, path: &str) -> Result<(), Box<Error>>{
        let mut writer = csv::Writer::from_path(path)?;

        for (u, v) in &self.0 {
            for (t, r) in v {
                writer.write_record(&[u.to_string(), t.to_string(), r.to_string()])?;
            }
        }

        Ok(())
    }
}

/// Get the number of tags present in an user file.
/// All the users must have the same number of tags.
/// The file must have at least one user with one tag.
fn get_tags_number(path: &str) -> Result<usize, Box<Error>> {
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(path)?;

    let mut records = reader.records();

    match records.next() {
        Some(record) => {
            let record = record?;
            let userid = i64::from_str_radix(record.get(0).unwrap(), 10)?;
            let mut ntags = 1;

            for record in reader.records() {
                let record = record?;

                let user = i64::from_str_radix(record.get(0).unwrap(), 10)?;

                if user != userid {
                    return Ok(ntags)
                } else {
                    ntags += 1;
                }
            }

            Ok(ntags)
        },
        None => {
            Err(Box::new(io::Error::new(ErrorKind::InvalidData, "Empty file")))
        }
    }


}

/// Merge multiple workers set together.
/// The user id must correspond to the same user in all the files and the tags should be complementary.
/// The files must be sorted by users and have the same users.
/// # Example :
/// - file 1 :
/// ```
/// user 1, tag 1, ratio
/// user 1, tag 2, ratio
/// user 2, tag 1, ratio
/// user 2, tag 2, ratio
/// ```
/// - file 2 :
/// ```
/// user 1, tag 3, ratio
/// user 2, tag 3, ratio
/// ```
/// - Result
/// ```
/// user 1, tag 1, ratio
/// user 1, tag 2, ratio
/// user 1, tag 3, ratio
/// user 2, tag 1, ratio
/// user 2, tag 2, ratio
/// user 2, tag 3, ratio
/// ```
pub fn merge_workers_files(paths_in: &[String], path_out: &str) -> Result<(), Box<Error>> {
    // initialize the readers
    let mut readers = vec![];

    for path in paths_in {
        let ntags = get_tags_number(path)?;

        let reader = csv::ReaderBuilder::new()
            .has_headers(false)
            .from_path(path)?;

        readers.push((reader.into_records(), ntags));
    }

    let mut writer = csv::Writer::from_path(path_out)?;
    let mut over = false;

    while ! over {
        for (records, ntags) in &mut readers {
            for i in 0..*ntags {
                match records.next() {
                    Some(record) => {
                        let record = record?;
                        writer.write_record(record.iter())?;
                    },
                    None => {
                        over = true;
                    }
                }
            }
        }
    }

    Ok(())
}