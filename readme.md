# So queries

Queries on the stackoverflow model

## Dataset

Data source : https://archive.org/download/stackexchange

    - Posts : https://archive.org/download/stackexchange/stackoverflow.com-Posts.7z
    - Tags : https://archive.org/download/stackexchange/stackoverflow.com-Tags.7z
    - Users : https://archive.org/download/stackexchange/stackoverflow.com-Users.7z
    - Votes : https://archive.org/download/stackexchange/stackoverflow.com-Votes.7z
    
Transform the XML files to CSV (see the scripts in ./src/cleaning). 


## Compile and run

This program is written with rust (version 1.32) and needs the rust toolchain (cargo) to be build.

```sh
# To compile this program :
cargo build --release
# To run the compiled program
./target/release/so_queries
```

## Actions

### Votes reductions

The vote table is heavy (+160M rows) and can be reduced by adding collumns to posts to count the correct number of votes.
This function will count the number of "Best answer" (vote type 1), "Up vote" (vote type 2) and "Down vote" (vote type 3) for each post and add 3 collumns to each post (one for each type of vote).

```sh
so_queries redvote -v path-votes-1 ... path-votes-n -p path-posts -o path-result
```


### Ratio UV

UV = Up votes  
DV = Down votes

ratio = UV / (UV + DV)

Calculate this ratio for each post (for each tag used in the post).
Then calculate the average ratio for each user per tag and write it to a CSV file.
Get global statistics per tag on the standard output.


```sh
# Generate workers set in path-workers and calculate statistics on it
so_queries gen -p path-posts -r path-posttags -w path-workers -t 3 17 9 5
```

